class keepass2::params {
  case $::operatingsystem {
    'ubuntu': { $package = ['keepass2', 'keepass2-doc'] }
    default: {fail("OS family ${::osfamily} not supported!")}
  }
}
