puppet-keepass2
--------------

Install KeePass2 on Ubuntu from ppa.

# License
Apache License, Version 2.0

# Contact
Nathan Valentine - nrvale0@gmail.com

# Support
Please log tickets and issues at [the project's site](http://github.com/nrvale0/puppet-keepass2).

